import '@google/model-viewer/dist/model-viewer'
import * as THREE from 'three';
import * as TWEEN from '@tweenjs/tween.js'
import { generateCard, generateCircleGround, removeAllChildren } from './utils/Utils';
import { GLTFLoader, OrbitControls } from 'three/examples/jsm/Addons.js';
import { TextureLoader } from 'three';


const app = document.getElementById('app');
const btnStartAR = document.getElementById('btn-start');
const cardContainer = document.getElementById('card-container');
const spinCardBtn = document.getElementById('spin');

const listCard: HTMLElement[] = [];
const numberRound = 16;
let currentRound = 0;
let spinning = false;

const flipCard = (card: HTMLElement, onChange? : (card: HTMLElement) => void, onComplete?: (card: HTMLElement) => void) => {
  const scale = new THREE.Vector2(0, 0);
  const tween = new TWEEN.Tween(scale).to({ x: Math.PI * 2, y: Math.PI * 2 }, 3000)
    .easing(TWEEN.Easing.Circular.InOut)
    .onUpdate((object: THREE.Vector2, time: number) => {
      card.style.transform = `rotateY(${object.y * 180 / Math.PI}deg)`;
      if (object.y >= Math.PI) {
        onChange && onChange(card);
      }
    })
    .onComplete((object: THREE.Vector2) => {
      onComplete && onComplete(card);
    }).start();
}

const chooseBorder = '5px solid orange';

for (let i = 0; i < 8; i++) {
  const card = generateCard();
  card.innerHTML = 'CARD ' + (i + 1);
  card.style.border = '1px solid black';
  cardContainer.appendChild(card);
  listCard.push(card);
}

const onSpinClick = (ev) => {
  if (spinning) {
    return;
  }
  spinning = true;
  const animateCard = () => {
    listCard.forEach(card => {
      card.style.border = '1px solid black';
    })
    let index = Math.floor(Math.random() * listCard.length);
    if (currentRound === numberRound - 1) {
      index = 5;
    }
    const card = listCard[index];
    card.style.border = chooseBorder;
    currentRound++;
    if (currentRound >= numberRound) {
      spinning = false;
      currentRound = 0;
      flipCard(card, (c: HTMLElement) => {
        c.innerHTML = 'Egg';
      }, (c: HTMLElement) => {
        
      });
      spinCardBtn.removeEventListener('click', onSpinClick);
      spinCardBtn.addEventListener('click', onSpinClick);
      return;
    }

    setTimeout(() => {
      animateCard();
    }, 100)
  }
  animateCard();
}

spinCardBtn.addEventListener('click', onSpinClick);


const gltfLoader = new GLTFLoader();
const textureLoader = new TextureLoader();

const modelViewerHtml = (glb: string, usdz: string): string => {
  return `<model-viewer id="model-viewer" src="${glb}" ios-src="${usdz}" scale="1 1 1"
  disable-zoom alt="" ar auto-rotate autoplay animation-name="idle" camera-controls>
</model-viewer>`
}

const gotoAR = (glb: string, usdz: string) => {
  removeAllChildren(app);
  app.insertAdjacentHTML('beforeend', modelViewerHtml(glb, usdz));
}


const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(app.clientWidth, app.clientHeight);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.toneMapping = THREE.CineonToneMapping;
renderer.toneMappingExposure = 1.6;
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
THREE.ColorManagement.enabled = false;

const scene = new THREE.Scene();
scene.background = new THREE.Color(0, 0.8, 1);
const camera = new THREE.PerspectiveCamera(45, app.clientWidth / app.clientHeight, 0.01, 1000);
camera.position.set(0, 1, 10);
camera.lookAt(new THREE.Vector3(0, 0, 0));

const orbitControl: OrbitControls = new OrbitControls(camera, renderer.domElement);
// orbitControl.target = new THREE.Vector3(0, 0, 0);
// orbitControl.enableDamping = true;
// orbitControl.enablePan = false;
// orbitControl.maxPolarAngle = Math.PI / 2.5;
// orbitControl.minPolarAngle = Math.PI / 4;

const ambientLight = new THREE.AmbientLight(0xffffff, 1);
scene.add(ambientLight);

const hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.6);
hemiLight.position.set(0, 10, 0);
scene.add(hemiLight);

const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
directionalLight.position.set(0, 1, 0);
directionalLight.shadow.mapSize.width = 2048  // default
directionalLight.shadow.mapSize.height = 2048  // default
directionalLight.shadow.camera.near = 0.1  // default
directionalLight.shadow.camera.far = 2000  // default
directionalLight.shadow.bias = -0.000002;
directionalLight.castShadow = true;
scene.add(directionalLight);

scene.add(generateCircleGround(2))

let animationMixer: THREE.AnimationMixer;
gltfLoader.load('assets/models/dragon_egg.glb', (data) => {
  if (data) {
    console.log(data.animations)
    data.scene.scale.set(3, 3, 3);
    data.scene.traverse(child => {
      if (child instanceof THREE.Mesh) {
        child.castShadow = true;
        child.receiveShadow = true;

      }
    })
    animationMixer = new THREE.AnimationMixer(data.scene)
    if (data.animations.length > 0) {
      const clip = data.animations[0]
      if (clip) {
        let action: THREE.AnimationAction = animationMixer.clipAction(clip)
        action.setLoop(THREE.LoopRepeat, 1);
        action.play()
      }
    }
    scene.add(data.scene);
  }
})

const update = (dt: number) => {
  orbitControl.update();
  renderer.render(scene, camera);
  animationMixer && animationMixer.update(dt);
}

let previousTime = Date.now();

const animate = () => {
  const currentTime = Date.now();
  const dt = currentTime - previousTime;
  TWEEN.update();
  update(dt / 1000);
  previousTime = currentTime;
  requestAnimationFrame(animate);
}
animate();
app.appendChild(renderer.domElement);

window.addEventListener('resize', () => {
  camera.aspect = app.clientWidth / app.clientHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(app.clientWidth, app.clientHeight);
});

btnStartAR.addEventListener('click', () => {
  gotoAR('assets/models/Astronaut.glb', 'assets/models/Astronaut.usdz');
  btnStartAR.hidden = true;
});