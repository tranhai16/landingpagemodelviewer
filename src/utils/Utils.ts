import * as THREE from 'three'
import * as TWEEN from '@tweenjs/tween.js'
export function removeAllChildren(element: HTMLElement): void {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

export function generateCircleGround(radius: number, color? : THREE.Color) : THREE.Mesh {
    let circleGeometry = new THREE.CircleGeometry(radius, 64);
    let circleMaterial = new THREE.MeshBasicMaterial({ color: color ? color : 0xffffff });
    const circle = new THREE.Mesh(circleGeometry, circleMaterial);
    circle.position.set(0, 0, 0);
    circle.rotation.x -= Math.PI / 2;
    circle.receiveShadow = true;
    return circle;
}

export function generateCard() : HTMLElement {
    const card = document.createElement('div');
    card.classList.add('card');

    // const front = document.createElement('div');
    // front.classList.add('card__side');
    // front.classList.add('card__side--front');

    // const back = document.createElement('div');
    // back.classList.add('card__side');
    // back.classList.add('card__side--back');

    // front.appendChild(frontContent);
    // back.appendChild(backContent);
    // card.appendChild(front);
    // card.appendChild(back);
    return card;
}